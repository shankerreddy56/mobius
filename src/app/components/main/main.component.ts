import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { SearchService } from 'src/app/services/search.service';

@Component({
  selector: 'main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css'],
})
export class MainComponent implements OnInit {
  constructor(private obj: SearchService) {}
  result: any;
  filteredString: any = '';

  p: number = 1;
  ngOnInit(): void {
    this.obj.getdata().subscribe(
      (res) => {
        this.result = res;
      },
      (err: HttpErrorResponse) => {
        if (err.error instanceof Error) {
          console.log('client side errors');
        } else {
          console.log('server side errors');
        }
      }
    );
  }
}
