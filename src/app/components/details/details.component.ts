import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { SearchService } from 'src/app/services/search.service';

@Component({
  selector: 'details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css'],
})
export class DetailsComponent implements OnInit {
  result: any;
  constructor(private obj: SearchService) {}

  ngOnInit() {
    this.obj.getdata().subscribe(
      (res) => {
        this.result = res;
      },
      (err: HttpErrorResponse) => {
        if (err.error instanceof Error) {
          console.log('client side errors');
        } else {
          console.log('server side errors');
        }
      }
    );
  }
}
