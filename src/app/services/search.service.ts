import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root',
})
export class SearchService {
  constructor(private obj: HttpClient) {}
  public getdata(): Observable<any> {
    return this.obj.get('https://api.github.com/search/users?q=eric');
  }
}
