import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { NgxPaginationModule } from 'ngx-pagination';

import { MainComponent } from './components/main/main.component';
import { FilterPipe } from './pipes/filter.pipe';
import { DetailsComponent } from './components/details/details.component';
import { RouterModule } from '@angular/router';
import { approutes, AppRoutingModule } from './app-routing.module';

@NgModule({
  declarations: [MainComponent, FilterPipe, DetailsComponent],
  imports: [
    AppRoutingModule,
    BrowserModule,
    HttpClientModule,
    FormsModule,
    Ng2SearchPipeModule,
    NgxPaginationModule,
    RouterModule.forRoot(approutes),
  ],
  providers: [],
  bootstrap: [MainComponent],
})
export class AppModule {}
